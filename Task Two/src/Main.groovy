class Main {

    static void main(String[] args) {
        def person1 = new Person("Joe", "Murphy", "14 Upper Hill, Hillside, Cork", "8978768448")
        def person2 = new Person("Mary", "Murphy", "14 Upper Hill, Hillside, Cork", "65465456654")
        def person4 = new Person("Mary", "Murphy", "12 Upper Hill, Hillside, Cork", "564654")
        def person3 = new Person("Bob", "O Neill", "24 Road Side, Upper Road, Waterford", "654654848564")

        def addressBook = new AddressBook(person1, person2, person3, person4)
        addressBook.mainMenu()
    }
}
