class LastNameComparator implements Comparator {
    @Override
    int compare(Object o1, Object o2) {
        def person1 = (Person) o1
        def person2 = (Person) o2
        return person1.lastName.compareToIgnoreCase(person2.lastName);
    }
}
