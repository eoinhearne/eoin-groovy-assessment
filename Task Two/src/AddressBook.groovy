class AddressBook {

    public ArrayList<Person> personList = new ArrayList<>()
    private Scanner input = new Scanner(System.in)

    AddressBook(Person... persons) {
        personList.addAll(persons)
    }

    void mainMenu() {

        String instructions = '\nType the number of menu item required and press enter'
        String menu = "Main Menu\n" +
                "1. Add person to address book.\n" +
                "2. Remove person from address book.\n" +
                "3. Edit person in address book.\n" +
                "4. View person's information.\n" +
                "5. View all people in address book\n" +
                instructions + "\n"

        println(menu)
        def menuChoice = input.nextInt()
        input.nextLine()
        switch(menuChoice) {
            case 1:
                addPersonMenu()
                break
            case 2:
                removePersonMenu()
                break
            case 3:
                editPersonMenu()
                break
            case 4:
                viewPersonMenu()
                break
            case 5:
                listAllMenu()
                break
            default:
                println "$menuChoice is not an option. Please try again."
                mainMenu()
        }
    }

    void addPersonMenu() {
        println "Add Person"
        println "Enter first name: "
        def firstName = input.nextLine()
        println "Enter last name: "
        def lastName = input.nextLine()
        println "Enter address: "
        def address = (String)input.nextLine()
        println "Enter phone number: "
        def phoneNo = (String) input.nextLine()

        addPerson(new Person(firstName, lastName, address, phoneNo))
        println()
        mainMenu()
    }

    void removePersonMenu() {
        println "Remove Person"
        //check if person exists and add to personFound list
        if(personList) {
            ArrayList<Person> personFound = findMultiPerson()
            if (personFound) {
                personFound.each{
                    println(it)
                    println()
                    println("Delete this person from address book? Y/N")
                    String answer = input.nextLine().toLowerCase()

                    while (answer != 'y' && answer != 'yes' && answer != 'n' && answer != 'no') {
                        println 'Enter Y or N only!'
                        answer = input.nextLine().toLowerCase()
                    }
                    if(answer.toLowerCase() == 'y' || answer.toLowerCase() == 'yes') {
                        removePerson(it)
                        println()
                        mainMenu()
                    }
                }
            } else {
                println 'No entries found.'
                mainMenu()
            }
            println 'No more entries found.'
            println()
            mainMenu()
        } else {
            addressBookEmpty()
        }
    }

    void listAllMenu() {
        listAll()
        def menu = "1. Sort by first name.\n" +
                "2. Sort by last name.\n" +
                "3. Exit to main menu."
        println (menu)
        int answer = input.nextInt()
        while(answer < 1 && answer > 3) {
            println "$answer is not a valid option!"
            answer = input.nextInt()
        }
        switch(answer) {
            case 1:
                listByFirstName()
                mainMenu()
                break
            case 2:
                listByLastName()
                mainMenu()
                break
            case 3:
                mainMenu()
                break
        }
        input.nextLine()
        mainMenu()
    }

    void editPersonMenu() {
        println 'Edit Person'
        //check if person exists and add to personFound list
        if(personList) {
            ArrayList<Person> personFound = findMultiPerson()
            if (personFound) {
                personFound.each {
                    println(it)
                    println()
                    println("1. Edit first name.\n" +
                            "2. Edit last name.\n" +
                            "3. Edit address.\n" +
                            "4. Edit phone number.\n" +
                            "5. Next entry.")
                    int answer = input.nextInt()
                    input.nextLine()

                    while (answer < 1 && answer > 5) {
                        println("$answer is not a valid option!")
                        answer = input.nextInt()
                    }
                    switch(answer) {
                        case 1:
                            println("Enter first name: ")
                            def firstName = input.nextLine()
                            updateFirstName(it, firstName)
                            mainMenu()
                            break
                        case 2:
                            println("Enter last name: ")
                            def lastName = input.nextLine()
                            updateLastName(it, lastName)
                            mainMenu()
                            break
                        case 3:
                            println("Enter address: ")
                            def address = input.nextLine()
                            updateAddress(it, address)
                            mainMenu()
                            break
                        case 4:
                            println("Enter phone number: ")
                            def phoneNo = input.nextLine()
                            updatePhoneNo(it, phoneNo)
                            mainMenu()
                            break
                        case 5:
                            break
                    }
                }
            } else {
                println("No entries found.")
                mainMenu()
            }
            println("No more entries found.")
            println()
            mainMenu()
        } else {
            addressBookEmpty()
        }
    }

    void viewPersonMenu() {
        println("View Person")
        //check if person exists and add to personFound list
        if(personList) {
            ArrayList<Person> personFound = findMultiPerson()
            if (personFound) {
                personFound.each{
                    println(it)
                    println()
                    println("Press enter for next person.")
                    input.nextLine()
                }
            } else {
                println("No entries found.")
                mainMenu()
            }
            println("No more entries found.")
            println()
            mainMenu()
        } else {
            addressBookEmpty()
        }
    }

    ArrayList<Person> findMultiPerson() {
        println("Enter first name: ")
        def firstName = input.nextLine()
        println("Enter last name: ")
        def lastName = input.nextLine()
        ArrayList<Person> foundPerson = new ArrayList<>()
        if (personList) {
            personList.each{
                if(it.firstName.compareToIgnoreCase(firstName) == 0) {
                    if (it.lastName.compareToIgnoreCase(lastName) == 0) {
                        foundPerson.add(it)
                    }
                }
            }
        }

        return foundPerson
    }

    void addPerson(Person person) {
        personList.add(person)
        println(person.getFullName() + ": successfully added to address book.")
    }

    void removePerson(Person person) {
        personList.remove(person)
        println(person.getFullName() + ": successfully removed from address book.")
    }

    static void updateFirstName(Person person, String newFirstName) {
        def oldName = person.getFullName()
        person.firstName = newFirstName
        println(person.getFullName() + ": first name successfully updated.")
        println("$oldName -> " + person.getFullName())
    }

    static void updateLastName(Person person, String newLastName) {
        def oldName = person.getFullName()
        person.lastName = newLastName
        println(person.getFullName() + ": last name successfully updated.")
        println("$oldName -> " + person.getFullName())
    }

    void updateAddress(Person person, String newAddress) {
        person.address = newAddress
        println(person.getFullName() + ": address successfully updated.")
        println(newAddress)
    }

    void updatePhoneNo(Person person, String newPhoneNo) {
        person.phoneNo = newPhoneNo
        println(person.getFullName() + ": phone number successfully updated.")
        println(newPhoneNo)
    }

    void listAll() {
        if(personList) {
            personList.each{
                println(it.getFullName())
            }
            println()
        } else {
            addressBookEmpty()
        }
    }

    void listByFirstName() {
        personList.sort(true, new FirstNameComparator())
        listAllMenu()
    }

    void listByLastName() {
        personList.sort(true, new LastNameComparator())
        listAllMenu()
    }

    void addressBookEmpty() {
        println("Address book is empty.")
    }
}
