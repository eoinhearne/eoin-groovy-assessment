class Person {

    def firstName
    def lastName
    def address
    def phoneNo

    Person(String firstName, String lastName, String address, String phoneNo) {
        this.firstName = firstName
        this.lastName = lastName
        this.address = address
        this.phoneNo = phoneNo
    }

    def getFullName() {
        return "$firstName $lastName"
    }

    @Override
    String toString() {
        return "Name: $firstName $lastName\nAddress: $address\nPhone no: $phoneNo"
    }
}
