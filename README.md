# Eoin Groovy Assessment

Task one includes classes to demonstrate an understanding of certain features of the Groovy programming language.

## Classes

### Main

This processes each class and prints the necessary data to demonstrate how each function works.

### Exercise4

How are implicit getters and setters created in Groovy?

### Exercise5

How do you stop a setter from being created? 

### Exercise8

How do I create a Map in Groovy?

### Exercise9

How do I create a List in Groovy? 

### Exercise10

How do I create a Set in Groovy?

### Exercise13

Demonstrate usage of the “.each” method on collections

### Exercise14

Use the “.find” and “.findAll” methods on collections to find objects by property values

### Exercise15

Write a simple Groovy application that will print the names of all files in a directory 

### Exercise16

Write a simple Groovy application that will print the names of all files in a directory and and subdirectories

### Exercise17

Using the Groovy MarkupBuilder, write a simple Groovy application that creates a one page HTML file with 1-2 text paragraphs about your hometown, as well as 2-3 images, and a number of links to local restaurant websites.