class Exercise10 {

    def mySet = [1, 2, 4, 10, 16, 24] as Set

    void printContents() {
        mySet.each {num ->
            print "$num "
        }
    }
}
