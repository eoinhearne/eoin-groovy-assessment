class Exercise8 {
    def myMap = [a:1, b:2, c:3]

    void printContents() {
        myMap.each{ key, value ->
            println "$key is the key; $value is the value"
        }
    }
}
