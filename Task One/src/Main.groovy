class Main {

    public static void main(String[] args) {
        println 'Exercise 4'
        def ex4 = new Exercise4("Bob", 24)
        println("$ex4.name is $ex4.age years old")
        ex4.setName("Mary")
        ex4.setAge(45)
        println("$ex4.name is $ex4.age years old")

        println()

        println 'Exercise 5'
        def ex5 = new Exercise5("Niall", 13)
        println("$ex5.name is $ex5.age years old")
        try {
            ex5.setName("David")
        } catch(ex) {
            println "Error for missing setter: $ex"
        }
        ex5.setAge(22)
        println("$ex5.name is $ex5.age years old")

        println()

        println 'Exercises 8'
        def ex8 = new Exercise8()
        ex8.printContents()

        println()

        println 'Exercises 9'
        def ex9 = new Exercise9()
        ex9.printContents()

        println()

        println 'Exercises 10'
        def ex10 = new Exercise10()
        ex10.printContents()

        println()
        println()

        println 'Exercises 13'
        def ex13 = new Exercise13()
        ex13.printAll()

        println()

        println 'Exercise 14'
        def ex14 = new Exercise14()
        ex14.printShortAnimals()
        println()
        ex14.animalBeginsWith("h")

        println()

        println 'Exercise 15'
        def ex15 = new Exercise15('.')
        ex15.printFilesInDirectory()

        println()

        println 'Exercise 16'
        def ex16 = new Exercise16('.')
        ex16.printAllFilesInDirectory()

        println()

        println 'Exercise 17'
        def ex17 = new Exercise17()
        ex17.createHTMLPage()
    }
}
