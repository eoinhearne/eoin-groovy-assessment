class Exercise14 {

    def animalList = ["dog", "cat", "horse", "bird"]

    void animalBeginsWith(String character) {
        println "Animal beginning with $character"
        def animal = animalList.find{ it.startsWith(character)}
        if(animal) {
            println animal
        } else
            println "None begin with $character"
    }

    void printShortAnimals() {
        println 'Small Animal Words'
        def smallAnimals = animalList.findAll { it.size() == 3}
        smallAnimals.each{ println it }
    }
}
