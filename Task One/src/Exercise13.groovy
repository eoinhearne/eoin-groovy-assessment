class Exercise13 {

    def myList = ["The Smiths", "Pixies", "Nirvana", "Hozier"]

    void printAll() {
        myList.each{ println it}
    }
}
