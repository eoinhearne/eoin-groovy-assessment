class Exercise16 {

    File file;
    String fileName;

    Exercise16(String fileName) {
        this.fileName = fileName
        this.file = new File(fileName)
    }

    void printAllFilesInDirectory() {
        file.eachFileRecurse{ println it }
    }


}
