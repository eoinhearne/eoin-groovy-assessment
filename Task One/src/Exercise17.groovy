import groovy.xml.MarkupBuilder

class Exercise17 {

    def builder = new MarkupBuilder()

    void createHTMLPage() {
        builder.html {
            body {
                h1('Waterford City')
                p("It’s Ireland’s oldest city, and despite boasting a history of over 1,000 years, " +
                        "Waterford just keeps coming out with more surprises. Ancient Viking creations, Norman " +
                        "constructs and glittering expanses of immaculately crafted crystal – step in to this " +
                        "glittering gem in Ireland's Ancient East, and discover the not-so-secret treasures that make it truly special.")
                p('At the heart and soul of Waterford is the world-renowned Waterford Crystal. Since 1783, the factory has been ' +
                        'crafting elegant crystal creations, and a host of craftsmen, glass blowers, cutters and engravers still ' +
                        'work in Waterford to this day. Every day, two tons of molten crystal are shaped into the impossible and ' +
                        'beautiful objects that give Waterford crystal its reputation for artistry around the globe.')
                img(style: 'max-width : 400px', src: 'https://upload.wikimedia.org/wikipedia/commons/b/b6/THE_WATCH_TOWER_IN_WATERFORD_CITY_%28PHOTOGRAPHED_MAY_2016%29_-_panoramio_-_William_Murphy_%282%29.jpg', alt: 'Image of Reginalds Tower')
                img(style: 'max-width : 400px', rc: 'https://upload.wikimedia.org/wikipedia/commons/7/79/Waterford_city_at_night_-_geograph.org.uk_-_1034017.jpg', alt: 'Image of Waterford City Centre')
                h2('Recommended restaurants')
                ul {
                    li{a(href: 'https://www.facebook.com/slicewoodfirepizza/', 'Slice')}
                    li{a(href: 'http://burzza.com/', 'Burzza')}
                    li{a(href: 'https://www.bodegawaterford.com/', 'Bodega')}
                    li{a(href: 'https://www.facebook.com/Emilianos-ristorante-italiano-167317383311641/', 'Emilianos')}
                    li{a(href: 'hhttp://www.mcdonalds.ie/iehome/promotions/trust-potatoes.html?gclid=EAIaIQobChMI49n1tKye5QIVD8jeCh0cVgXdEAAYASAAEgKeQfD_BwE', 'Mc Donalds')}
                }
            }
        }
    }
}
