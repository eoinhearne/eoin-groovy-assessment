class Exercise15 {

    File file;
    String fileName;

    Exercise15(String fileName) {
        this.fileName = fileName
        this.file = new File(fileName)
    }

    void printFilesInDirectory() {
        file.eachFile{ println it.getName() }
    }


}
