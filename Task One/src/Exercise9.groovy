class Exercise9 {
    def myList = ['dog', 'cat', 'duck', 'bird']

    void printContents() {
        myList.each{ animal ->
            println animal
        }
    }
}
